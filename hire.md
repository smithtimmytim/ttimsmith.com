---
layout: page
title: Hire Me
permalink: /hire/
---

I'm Tim Smith. I'm a designer, front-end developer, and broadcaster living in Saint Paul, Minnesota. I currently work as a freelancer. Take a look at [some of my recent work](/portfolio/) and [code I've written](https://github.com/ttimsmith). If you're curious, here's [my resumé](/resume/).

*I'd love to help you with UI/Web Design, Front-End Development, and Podcast Consulting.*{:.highlighted}

### Rates
My weekly rate is *$1600*{:.highlighted}.[^1] Depending on your circumstances, we can decide on a flat-rate for the project. I require one week upfront. Every week thereafter is paid on Monday.

### Availability
I'm currently booking projects for Q1 2015.

### Legal
You’ll need to sign my consulting agreement. The language is simple and easy to understand. If you need me to sign an agreement, I'll be happy to do so.

<a href="mailto:smith@ttimsmith.com?subject=Let’s Work Together" class="btn">Get in Touch</a>


[^1]: If you require less than a week of my time, please [get in touch!](mailto:smith@ttimsmith.com?subject=I Need Less Than a Week) We'll definitely work something out.
