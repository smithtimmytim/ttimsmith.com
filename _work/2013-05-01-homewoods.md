---
layout: portfolio
title: Homewoods on the Willamette
hero: uploads/2013/04/homewoods-full.png
role: Design, Front-End Development
type: Website
item_url: http://homewoods.org
excerpt: Homewoods on the Willamette is a beautiful retirement community located just outside Portland.
thumbnail: /uploads/2013/04/homewoods-thumb.png
date: '2013-05-01 13:27:44'
---

While working at Rocket Lift, I had the chance of working on a redesign for Homewoods on the Willamette. Homewoods is a gorgeous retirement community outside of Portland.

>I was impressed by how well [Tim] “got” us. He did not have the advantage of a physical visit to our beautiful property, but he seemed to fall in love with us and helped to show our best side at every opportunity.
<small><strong>Laura Engle</strong>Executive Director, Homewoods on the Willamette</small>
{: .content__pullquote }

When preparing to do this project, I did extensive research on the competition and saw that a lot of retirement homes were using stock photography, and keyword-injected copy, which to me, missed the mark of what people want to see when choosing where to retire. Thankfully, Laura Engle, the Executive Director at Homewoods felt the same. She wanted something real; something that told the Homewoods story, and conveyed the very warm community that exists.

All in all, I'm very happy with the result, and it brings me joy to see the Homewoods residents have welcomed the new site.
