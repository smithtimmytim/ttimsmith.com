---
layout: portfolio
title: Clinton Foundation
hero: uploads/2013/08/clinton-full.png
role: Front-End Development
type: Mobile Web App
excerpt: I helped the Clinton Foundation—together with the help of Crush + Lovely—to create a mobile Safari app for their visit to Africa.
thumbnail: /uploads/2013/08/clinton-thumb.png
date: '2013-07-05 13:27:44'
featured: affirmative
---
I was honored to be approached by [Crush + Lovely](http://crushlovely.com/) to help with a site for the Clinton Foundation. I was tasked with the front-end development of the app.

The Clinton's had organized a trip to Africa, and wanted a site that would serve as an information source for the visit. It would have the itinerary, allow trip-goers to post and view photos, and have bios for everyone on the visit.

We had a tight deadline, and to my delight, we finished early, which gave us more time for a thorough QA process. I've only used GitHub Issues once for bug tracking, but I just fell in love with it after this project. Since then, we've moved to using GitHub exclusively on projects we initiate at [Anythin' Goes](http://anythingo.es).

I worked together with such a talented team at Crush + Lovely, and it was great to hear that the Clinton Foundation was happy with the work we had done. I have to give a huge shout-out to [Jeff Schram](https://twitter.com/jeffschram) who I credit with changing the way I organize my Sass, for the better.

I'm very grateful to Crush + Lovely for having me work on this project with them.
