---
layout: portfolio
title: Drupalize.Me
thumbnail: /uploads/2012/12/drupalize-thumb.png
hero: uploads/2012/12/drupalize-full.png
role: User Experience, UI Design
type: Web App
item_url: http://drupalize.me/
excerpt: Learn Drupal from the experts where you get over 231 hours of Drupal training. You can see these videos from anywhere with our apps for iOS, Android and Roku.
date: '2012-09-03 13:27:44'
---
While working at [Lullabot](http://www.lullabot.com/), I was also put on the Drupalize.Me project. Drupalize.Me is a product created by Lullabot in an effort to teach people [Drupal](http://drupal.org/).

Drupal is a complex, yet very powerful content management system. Some of the biggest sites on the internet are powered by Drupal. Sites like [whitehouse.gov](http://www.whitehouse.gov), [grammy.com](http://www.grammy.com) and more are all made with Drupal.

However, as with most things that are powerful, Drupal can have a steep learning curve, leaving many beginners wondering where to start. That's where Drupalize.Me comes in. Boasting a catalog of more than 242 hours of Drupal training, many attribute their Drupal skills to this great site.

With the direction of [Jared Ponchot](http://www.lullabot.com/about/team/jared-ponchot), I took care of many design tasks that were needed and I also had the privilege of leading the redesign of this great resource.

You can read more about my process and work on the redesign, [here](http://drupalize.me/blog/201207/giving-drupalizeme-new-coat-paint).
