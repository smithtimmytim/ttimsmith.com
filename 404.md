---
layout: page
title: Doh! Page not found…
permalink: /404/
---

Should something be here? [File an issue on GitHub](https://github.com/ttimsmith/new-ttimsmith).

![Cat Mittens]({{ site.url }}/uploads/2013/08/cat-mittens.gif)
