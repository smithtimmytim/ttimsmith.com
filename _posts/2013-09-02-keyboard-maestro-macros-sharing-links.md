---
layout: post
title: 'Keyboard Maestro Macros: Sharing Links'
categories: articles
custom_type: post
date: "2013-09-02 16:40:14"
hidden: true
---
I've been using Keyboard Maestro for about two weeks, and I've enjoyed how much time it's starting to save me.

Two new Keyboard Maestro Macros I've made are for sharing links. I often come across things on the web that I want to share and I have to copy the title, switch to Tweetbot, paste the tile, switch back to Chrome, copy the URL, switch back to Tweetbot, and paste the URL. This get's irritating, so I created a Macro two copy these two values and put them on my clipboard.

![Tweet Sharing Macro](/uploads/2013/09/macro-1.png)

I believe Safari has built in sharing, but that still sucks because you have to click. This uses a keyboard shortcut.

Another thing I do often is copy links for show notes. For most podcasters, show notes are the worst part of doing the show. It's difficult to get the link quick and save it somewhere. I personally envy the hosts on 5by5 that have a bookmarklet. However, I've solved this problem quite easily for me with another macro.

![Markdown Sharing Link](/uploads/2013/09/macro-2.png)

So easy! I press `Ctrl+Opt+M` and the link is in my clipboard formatted as Markdown. The time this saves me! I used to use a Chrome Extension, but again, I try to use the mouse as little as possible.

Hope these can save you some time. If you've never used [Keyboard Maestro](http://www.keyboardmaestro.com/main/), I recommend you look into it. Keyboard shortcuts FTW!
