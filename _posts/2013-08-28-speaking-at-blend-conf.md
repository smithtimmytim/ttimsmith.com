---
layout: post
title: Speaking at Blend Conf
categories: links
custom_type: link
custom_url: http://www.blendconf.com
date: "2013-08-28 15:10:51"
hidden: true
---
Next week, I'll be flying to Charlotte, North Carolina, to give my second-ever conference talk. That I'm nervous would be an understatement. But, I'm very passionate about the topic,[^1] and can't wait to talk about it.

I want to take a moment to thank [Bermon Painter](http://bermonpainter.com/) and Blend Conf. He invited me to speak even before I had [proof that I could speak](/2013/05/become-a-better-designer-with-side-projects/) at a venue. I really appreciate that. Not to mention, Blend Conf has been our primary sponsor since July, and I'm very grateful for that as well. Without Blend Conf, we could not have kept doing the show.

For those of you going, please [get in touch](https://twitter.com/ttimsmith)! We should grab lunch, dinner, or something. It'll be lots of fun. If by any chance you've been waiting to get a ticket, do it now! I believe there are only 20 tickets left, and remember you can use `timsentme` for 20% off. If you can't make it, Bermon has an [Indiegogo Campaign](http://www.indiegogo.com/projects/help-us-record-40-amazing-sessions-at-blend-conference) to get all the sessions recorded. Then you can watch them from your couch. How cool is that?

Hope to see you next week!


[^1]: I'll be talking about side-projects and how they make you a better designer. Here's the [full schedule](http://www.blendconf.com/schedule/).
