---
layout: post
title: Dear Readers,
categories: articles
custom_type: post
date: "2013-09-16 08:44:33"
hidden: true
---
As you may have noticed in the past couple of weeks, I've taken the responsibility of writing on this site a bit more seriously. If you haven't noticed, that's ok, I'm telling you now.

Since [moving over to Jekyll](/2013/08/moving-over-to-jekyll/), the ability to write and publish quickly has given me fewer excuses not to write. I know that whether people read or not, it's important to maintain some type of documentation of life and work.

For years, I've stressed about writing things that would be "interesting"; judging at every turn whether people would like what I had to say. More recently, I've been persuading myself not to care. I write and share my opinion on things that interest me, and call it a day. To my surprise, there are those who appreciate that.

I'm also interested in becoming a better writer, and after doing research, I found that as with most things, the best way is to practice. I look up to writers like [Marco Arment](http://www.marco.org/), [Shawn Blanc](http://shawnblanc.net/), [Sara Wachter-Boettcher](http://sarawb.com/), [Adam Clark](http://www.avclark.com/), and draw inspiration from how they write. What I love especially about Sara and Adam is the level of honesty they're able to communicate with their writing. I admire that vulnerability.

So here's to a new beginning with this site. I hope you'll be part of it. You can now subscribe to the [master feed](http://ttimsmith.com/feed.xml) or an [articles-only feed](http://ttimsmith.com/feed.article.xml). All of the posts are shared on [Twitter](https://twitter.com/ttimsmith_com) and [App.net](https://alpha.app.net/ttimsmith_com), and if you prefer email, [there's an option for that](/newsletter) too.

Please [say hello](mailto:smith@ttimsmith.com). I love hearing from you guys.

Thanks for reading.

—Tim

