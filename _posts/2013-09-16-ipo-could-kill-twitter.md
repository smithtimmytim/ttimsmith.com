---
layout: post
title: IPO Could Kill Twitter
categories: links
custom_type: link
custom_url: http://www.wired.com/business/2013/09/killer-ipo-could-kill-twitter/
excerpt: This is what scares me about Twitter going public. As a private company, they've already proven to not give a damn about users, and the pressure of shareholders could make it worse.
date: "2013-09-16 10:35:00"
hidden: true
---
Marcus Wohlsen from Wired:

>But once it goes public, Twitter will have no choice but to strive to maximize shareholder returns, which would appear to create a Catch-22. More ads on Twitter means more money for Twitter, which makes shareholders happy. But more ads on Twitter will make users less happy, which means fewer users. Fewer users mean lower ad rates, which makes shareholders unhappy — a vicious cycle.

This is what scares me about Twitter going public. As a private company, they've already proven to not give a damn about users, and the pressure of shareholders could make it worse. They'll serve us just enough ads so we hate them, but not enough so we leave.

I see a very grim future for Twitter clients. Unless these developers budge and allow for sponsored tweets in the timeline, I don't see how Twitter will allow them to exist. I use Tweetbot exclusively, which can't be good for Twitter in terms of selling ads and users actually seeing them.

The real shame in all of this, is that a platform like App.net will never be as popular for the masses. Which is unfortunate because they ***actually have a freaking business model***.
