---
layout: post
title: A Week of Better Sleep
categories: articles
custom_type: post
hidden: true
---
This past week, I forced myself into a sane sleeping schedule and loved every moment of it.

I've been struggling to live healthier for about two years. When I lived in my hometown, I would play basketball for about three hours, twice a week, and my sleep schedule was a lot better.

I work from a chair everyday for 8–9 hours a day. I had gotten in the habit of buying food for lunch and dinner, thinking that I didn't have time to prepare a lunch, and feeling tired after a day of work to cook dinner.

My habits scare me, and recently, I've started to take my health a lot more seriously. One of the many things I need to work on is a better sleeping schedule, and last week I [began an experiment](/2013/08/my-sleeping-schedule-sucks) to make this better. I had some people try the experiment with me,[^1] which really helped.

I went to sleep everyday before midnight, except for one day; ate exclusively at home, except for one meal, and walked 4 out of 5 days. I feel well rested, more productive, and although technically I was awake for the same amount of hours, the day felt longer.

I'm so glad I tried this experiment, and I plan to continue for the foreseeable future.[^2] I'm adding a couple of things to the experiment this week:

1. Eat fresh fruit and/or vegetables every day.
2. When eating out, eat only half of the portion, and take the rest home.
3. Read a book, or something useful before going to sleep.

I think it could be a lot of fun to add small things like this to the experiment every week. There's a lot work to be done, but from this small experience, I've seen you can accomplish a lot by making small steps in the right direction.

##### Further Reading
[The Pastry Box Project, 10 August 2013, baked by Geri Coady](http://the-pastry-box-project.net/geri-coady/2013-august-10/)

[^1]: I want to thank [Chris Frees](https://twitter.com/chris_frees) and [Kyle Roderick](http://www.kyleroderick.com/) for doing the experiment with me (or at least telling me they were).

[^2]: For normal people, my experiment is just their life.
