---
layout: post
title: Keyboard Maestro is Pretty Awesome
categories: links
custom_type: link
custom_url: http://brooksreview.net/2011/03/cmd-one/
alias: /links/keyboard-maestro-is-pretty-awesome
hidden: true
---
Yesterday, I went on a mission to figure out how to create a new link post, populate `custom_url` with a url, and create a `<blockquote>` with selected text. I was looking at my options, and it looked like I'd have to do something with server-side scripting. Thankfully, that's not the case. Thanks to [Ben Brooks](http://brooksreview.net/), I've learned how to create a simple keyboard shortcut that does exactly that.

In the process, I also bumped into [Keyboard Maestro](http://www.keyboardmaestro.com/main/) again, which I had heard about before, but never appreciated how powerful it was(I know that for some, I'm really late to the party here). If you haven't heard of this app, I recommend you take a look. For those of you who write Markdown, I think you'll enjoy [this screencast](http://www.macosxscreencasts.com/general/keyboard-maestro-markdown-library/).

I'll be posting a screencast on all this in the next couple of days.
