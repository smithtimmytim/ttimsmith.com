---
layout: post
title: Apple TV Updated with New Channels
categories: links
custom_type: link
custom_url: http://9to5mac.com/2013/08/27/apple-tv-updated-with-vevo-disney-channel-disney-xd-weather-channel-smithsonian-apps/
date: "2013-08-27 11:20:09"
hidden: true
---
>Today, Apple has issued an over-the-air update to the Apple TV that brings several new content apps. Notably, in line with expectations, an app for the Vevo Music Video service has arrived. Also new are Disney Channel and Disney XD apps.

It's been interesting to watch what Apple is doing with Apple TV. Will they release an SDK? "Analysts" have been predicting that for years, and nothing.

I won't speculate too much because I'm no good at predicting this type of stuff. What I will say, is that the Apple TV has [gotten a lot of attention](http://www.theverge.com/2013/6/19/4444814/apple-tv-adds-hbo-go-watchespn-others) recently. The device sells very well, even without being marketed in any way, and from a [recent report](http://www.macrumors.com/2013/06/10/app-store-developer-payout-over-10-billion-apple-stores-seeing-1-million-visitors-daily/), Apple has over 575 million iTunes accounts that it could sell content to. No one else, ***no one***, has that type of access to wallets.

The people want a better way to watch television, and I not only think Apple TV has the power to be that future, but also open up to gaming.[^1]

I said I wouldn't speculate, but I did anyway. The future of TV is one that really interests me, and if anyone has the money, and the customer base to do it, it'd be Apple.

[^1]: Which would be so easy to do, considering the infrastructure Apple has built around Game Center.
