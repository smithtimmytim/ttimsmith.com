---
layout: post
title: Sublime Text Keyboard Shortcuts
categories: links
custom_type: link
custom_url: http://www.ractoon.com/2012/10/sublime-text-2-keyboard-shortcuts-printable
date: "2013-08-24 20:41:26"
alias: /links/sublime-text-keyboard-shortcuts
hidden: true
---
[Sublime Text](http://www.sublimetext.com/) is my text editor of choice, and it seems that most of us use it these days. I love streamlining my workflow, so I was naturally very excited to see this guide of keyboard shortcuts.

For those of you not on a Mac, there's also a guide for Windows/Linux users. Now you can't say I don't think of you too.
