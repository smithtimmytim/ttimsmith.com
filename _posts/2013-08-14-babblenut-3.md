---
layout: post
title: 'Babblenut: It’s Gotta Be Something in the Water'
categories: links
custom_type: link
custom_url: http://babblenut.com/ep/3
date: "2013-08-14 19:42:37"
alias: /links/babblenut-3
hidden: true
---
This week: Paul and I talk about legacy, it's importance, and how we should pay attention to preserving it. Such a deep topic, and really, a lot to think about.

*Sponsored by [Blend Conference](http://blendconf.com) - Use code ‘timsentme’ at checkout for 20% off.*
