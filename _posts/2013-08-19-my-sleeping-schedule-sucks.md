---
layout: post
title: My Sleeping Schedule Sucks
categories: articles
custom_type: post
date: "2013-08-19 16:11:35"
alias: /articles/my-sleeping-schedule-sucks
hidden: true
---
I have a friend that has a "sleeping schedule". This is so important to her, that she's very vocal when it's being messed with.

I wish I could be like that: have a time I go to sleep, and a time I wake up. I think it's the difference to a healthier life that I can't seem to get ahold of. The times when I get to sleep at a reasonable hour[^1], I feel a lot better about the day, myself, the work I produce, and more.

You'd think that after experiencing a day like that, I would want that all the time. But there I am, day after day, still wide awake at 2 AM. This also means that I wake up around 10am (sometimes later), and that I'm normally ending my work day around 8pm.

### What I think this affects
When I get good sleep, I notice I'm a lot more motivated. I go on a walk, and I tend to eat healthier. For years, I've been saying that I'm a "night owl", and I'm beginning to think it's crap.

It's not that I don't function. I still get a lot done, but I feel like I could be so much more if I want to sleep at normal times. Who knows? Maybe that's not true. Maybe the human body can adapt to any schedule? Maybe this is just another one of those things that you think would make life so much better, but doesn't make a difference. I'm sure there's actual scientific data for all this.[^2]

### Why is this important?
Something I'm thinking about more, and more is health. I think that as an industry we've been thinking about it a bit more, considering that we spend a lot of time in front of the computer. Personally, I find it very difficult to balance life with healthy food, exercise, responsibilities, and work. I love what I do, and it's difficult to fit everything in.

I'm pretty young at the moment, but I feel that right now is the time to correct these habits, which I'm sure will come to bite me in the butt later on.

### Now for an experiment
So, since I think this might change my whole life, I'm going to do an experiment this week. This is what I'm going to do:

1. Go to sleep by 11:30pm
2. Wake up at 7am
3. Don't check Twitter or Email in bed
4. Don't eat out
5. Go for a walk everyday at 5pm

I'll check-in with you in a week to let you know how it went. Let me know if you'd like to do the experiment with me.

[^1]: Defined by most of the world as sometime in between 10–12 midnight.
[^2]: I'm not about to look it up though.
