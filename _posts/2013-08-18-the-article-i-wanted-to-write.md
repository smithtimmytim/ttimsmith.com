---
layout: post
title: The Article I Wanted to Write
categories: links
custom_type: link
custom_url: http://uptake.co/kind-of-a-big-deal
alias: /links/the-article-i-wanted-to-write
hidden: true
---
>Anyone who follows me on Twitter, has emailed with me or maybe chatted for a couple minutes at an event knows that I’m a pretty approachable person. What’s surprising to me is that kindness is a standout characteristic in a person. Shouldn’t it be the default?

This is the article I've been wanting to write. I just couldn't express it the right way. Brad does a great job of explaining that there are three ways to be a big deal:

1. Be kind
2. Help people
3. Repeat

I couldn't agree more. Like [Justin Mezzell](http://justinmezzell.com) and I talked about on [The East Wing](http://theeastwing.net/episodes/66), we need more voices like this within our community. We need positive voices—not one's with empty compliments—but with sincere positivity, and constructive criticism.

Read the article. It's a good read.
