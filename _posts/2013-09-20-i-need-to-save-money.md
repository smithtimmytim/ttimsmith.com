---
layout: post
title: 'Save Your Money, or Go Out of Business'
categories: links
custom_type: link
custom_url: https://lessaccounting.com/blog/going-out-of-business
hidden: true
---
>Save your money. A small business/freelancer needs cash. Keep at least three months of overhead sitting in a savings account. Then personally you need at least two months of living costs in a savings account.

What an honest post. I'm historically horrible at saving money, and yet as a freelancer, I know I should have at least 5 months of savings. I don't think I've ever had that saved in my life, even when I was making good money.

Save your money peeps!
