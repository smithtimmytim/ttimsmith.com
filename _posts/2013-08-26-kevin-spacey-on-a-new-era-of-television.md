---
layout: post
title: Kevin Spacey on a New Era of Television
categories: links
custom_type: link
custom_url: http://www.theverge.com/2013/8/26/4659904/kevin-spacey-on-netflix-we-have-learned-the-lesson-that-the-music-industry-didnt
hidden: true
---
>"we have demonstrated that we have learned the lesson that the music industry didn't learn. Give people what they want, when they want it, in the form they want it in, at a reasonable price — and they'll more likely pay for it rather than steal it."

I've always liked Kevin Spacey, and this quote from a recent speech he gave at the Edinburgh International Television Festival —makes me like him more.

It's refreshing to see that some people understand what television should become and I only wish that fat-cat TV execs would jump on the bandwagon. I don't think they realize how much money they miss out on by not embracing technology, and serving content through the medium's people want it in.

The video is only a bit under 5 minutes. Totally worth watching.
