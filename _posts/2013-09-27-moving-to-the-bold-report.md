---
layout: post
title: Moving to The Bold Report
categories: articles
custom_type: post
date: '2013-09-27 12:42:44'
hidden: true
---
Earlier this week, I launched my latest project, *[The Bold Report](http://theboldreport.net/)*.

For some time now, I'd been internally debating whether this site was the right place to keep my writing. I had increased my writing efforts, made it easier for people to subscribe, and I just wasn't seeing much growth.

At first, I came to the conclusion that it just hadn't been enough time. However, as time went on, I realized that the message of my personal site was confusing, and potential subscribers couldn't easily find out what they were reading, and therefore less likely to subscribe. My writing and portfolio deserved exclusive focus.

This is where *The Bold Report* comes in. Starting this week, that will be the home for all of my writing. In terms of content, there really is no change. If you've enjoyed the links, and longer articles, I'm sure you'll love *The Bold Report*. I'm also expanding into some other areas, and I look forward to hearing your thoughts on that.

### What will happen to the archives?
All of the content currently on this site, will remain here. In the next couple weeks, it will disappear from the homepage, but you'll still be able to look through the archives, and search the site. If you feel something has been deleted or misplaced, [file a GitHub issue](https://github.com/ttimsmith/ttimsmith.com), and I will do my best to fix it.

### How to support ***The Bold Report***
I would really appreciate if you [subscribed](http://theboldreport.net/subscribe/). It's a new site, none of the archives of this site will be moved, and for that reason, I won't be redirecting the RSS feeds. Just like here, I've made a master feed, an articles-only feed, and a [Twitter](https://twitter.com/theboldreport) and [App.net](https://alpha.app.net/theboldreport) account. If there's another way you like to receive updates, let me know and I might be able to accommodate you.

In the future, I hope to do a membership drive, together with some giveaways so that the site begins to make some money. I'm committed to delivering the best content I can, but I also need it to help me pay the bills. If you'd be interested in a membership eventually, please tell me. It allows me to gauge interest.

### Wrapping up
I started writing on this site when I was 16[^1]. Over the years, I've done many experiments on here. I've broken the site many times, designed awesome art-directed articles, and written foolish and idiotic opinions. Some of those still exist, and others, I've stupidly deleted. It's been an exciting journey, and if nothing else, it's been great to see myself grow, and learn.

Most of all, I want to say thank you. Thank you for supporting my site. Thank you for reading the articles. Thank you for reading, even when I had no idea what I was talking about.[^2] I appreciate it immensely.[^3]

This is a new beginning, and I'm very excited about it. Hope to see you there.

[^1]: The first version of this site was made in a college classroom with Dreamweaver. I bought the domain with my mother's debit card. I hosted it for free on GoDaddy. Unfortunately, I don't have screenshots of that site, but the color scheme was orange and grey, and the typeface was Arial. I think I did the world a favor by not keeping records of it.
[^2]: Which still happens!
[^3]: I started thinking about this post on Monday. In fact, I was supposed to publish it that day. I didn't realize how sad it made me though. This site has seen me grow so much: from a kid to a man. Maybe I'm taking it too seriously, but I will miss writing on here.
