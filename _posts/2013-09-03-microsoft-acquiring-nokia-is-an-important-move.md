---
layout: post
title: Microsoft Acquiring Nokia is an Important Move
categories: articles
custom_type: post
date: "2013-09-03 11:31:51"
---
Microsoft's Nokia acquisition is more important than you think. It's being brushed off by many in the media, and definitely by a lot of Apple fans; we love to ridicule this type of news. [Some just downright think it's a terrible idea](http://gigaom.com/2013/09/03/why-i-think-the-7-2-billion-microsoft-nokia-deal-is-a-terrible-idea/).

One thing is definitely true, the move is late. This acquisition would've interested a lot more people a couple of years ago. The mobile market is very competitive, Apple and Google have years of experience, and it seems Amazon wants a piece of the pie.

Microsoft and Nokia may be a joke in terms of market share[^1], but them joining forces is important news. The fact that hardware and software are now being done semi-in-house is a big deal, and indicative of some type of vision which could be successful[^2]. Microsoft has noticed something very important, and I talked about this when I wrote about [Scott Forstall becoming CEO](/2013/08/why-scott-forstall-should-be-microsofts-next-ceo/)—being responsible for the complete experience is the key to making products people want to use.

Since its inception, the company has allowed others to create the hardware for their software[^3]. They've given up control of the experience, and I feel it's one of the biggest reasons they aren't the company they want to be. Nokia becoming part of Microsoft begins to fix that.

**Microsoft doesn't want to be *just* a software company.**

In Steve Ballmer's letter to employees, he said, "We are committed to working with partners, helping them build great products and great businesses on our platform, and we believe this deal will increase our partner value proposition over time." I predict that in 4 years, Microsoft will be exclusively making Windows Phone. It'll be theirs; software and hardware.

One of the things that makes Apple so successful is the way they control the ecosystem. Lots of power users are turned away by how little control they have, but to the normal consumer, the careful curation of an ecosystem, makes the experience more enjoyable. It's also important to note, that companies that don't exercise this type of control [have lots of security problems](http://bits.blogs.nytimes.com/2013/08/28/u-s-government-issues-warning-about-security-on-android-phones/).

The next year and a half will tell whether Microsoft will take this opportunity, and execute well on it. They need a new phone, running a better operating system. If Microsoft is at all successful in the next 5 years, it will be in part because of this move.

[^1]: According to [this article](http://http://www.theverge.com/2013/9/3/4688962/with-nokia-microsoft-wants-to-triple-windows-phone-marketshare-by-2018), Microsoft's marketshare is just over 4% in the smartphone market.

[^2]: I think the [Nokia Lumia](http://www.nokia.com/us-en/phones/phone/lumia1020/) is a great example of some great hardware decisions. It's a pretty good-looking phone, and has a 41 megapixel camera which is perfect for what people want to do with their phone: take pictures of themselves and food.

[^3]: This is the reason you spend hours uninstalling software from a PC you just bought. Android has this same problem by allowing carriers to make different versions of it.