---
layout: post
title: 1Password 4 Preview
categories: links
custom_type: link
custom_url: http://www.macworld.com/article/2047335/agilebits-teases-significant-revamp-of-1password-for-os-x.html
date: "2013-08-24 23:32:06"
hidden: true
---
>While Apple’s announcement of the forthcoming iCloud Keychain feature of iOS 7 and OS X Mavericks might have seemed like dire news for products like AgileBits’s 1Password, the latter company’s not about to throw in the towel.

Here I go again, advertising for non-sponsors, but I don't care. 1Password has been such a huge time saver, and workflow improver for me, it's my pleasure to talk about them.

If you have 1Password 4 for iOS, you'll notice the similarities immediately. I'm really glad they're bringing the design of their iOS app to the Mac App, I can't possibly be the only one to think it needs some love. The release is scheduled for this Fall. If you don't use 1Password, get in touch with me, you have no idea what you're missing out on.
