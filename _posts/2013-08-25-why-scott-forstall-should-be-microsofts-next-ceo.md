---
layout: post
title: Why Scott Forstall Should Be Microsoft’s Next CEO
categories: articles
custom_type: post
date: "2013-08-25 00:18:10"
---
Scott Forstall could give Microsoft exactly what it's been missing.

I for one don't understand all the hate for Scott Forstall. Is the man a jerk? Maybe. Is he difficult to work with? From what I hear, yes. However, he isn't given enough credit for what he did at Apple.

Forstall is a brilliant engineer, and although many don't necessarily agree with his design sense, his vision for iOS made the platform what it was, and easy to use.[^1]

Microsoft is in trouble. They've missed out on several opportunities, and fired Steve Ballmer *really late*.[^2] Scott comes from a company that cares about design, and understands the benefit of being responsible for both the software and hardware. Microsoft has historically been horrible in this area.

I'm not the only one [saying this](http://www.loopinsight.com/2013/08/23/apple-executives-as-microsoft-ceo/). 

This is a prime opportunity for Microsoft. They can make a comeback and create some products that they're actually proud of, instead of making jealousy-ridden commercials that take cheap shots at Apple. 

This is a really great opportunity for Forstall too. Stick it to Apple and make something great somewhere else. Microsoft doesn't have to be Apple, but Scott can bring certain philosophies that the company desperately needs. Scrap everything, start fresh, and start thinking differently.

I find myself weirdly rooting for Microsoft here. Could Microsoft be cool? Could they finally make something that billions of people use, but don't hate? Yes.

[^1]: I'm not yet convinced on Jony being in charge of software design too.
[^2]: Seriously. I can't believe this didn't happen earlier.
