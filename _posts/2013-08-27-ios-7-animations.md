---
layout: post
title: iOS 7 Animations
categories: links
custom_type: link
custom_url: http://www.marco.org/2013/08/27/along-for-the-ride
date: "2013-08-27 23:04:02"
hidden: true
---
>These animations in iOS 7 feel like its designers are showing off their cool new abilities, and we’re just along for the ride. After sitting through all of these, day after day, it’s no longer impressive — it just feels needlessly, artificially slow.

This is what's so interesting about interface animations: they need to be just right. Too slow, and the UI feels sluggish. Too fast, and the UI feels abrupt.

All being said, I'm very surprised with the polish and improvements that have been made throughout the beta process. There's a lot of work to be done, but I'm confident that developers will push the platform forward as time goes by.
