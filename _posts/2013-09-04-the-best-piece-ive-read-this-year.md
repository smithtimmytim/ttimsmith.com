---
layout: post
title: The Best Piece I’ve Read This Year
categories: links
custom_type: link
custom_url: http://avclark.com
date: "2013-09-04 16:50:32"
hidden: true
---
>Every time I open up Photoshop or IA Writer or sit down at the drum kit, a cloud of angst settles around me as I set out to create or write something people will find impressive. And not just impressive, but so damn good, it will be linked to thousands of times and retweeted for all eternity.

Adam wrote so many things that I just didn't know how to communicate.

>I never ship because I can't face the potential of failure. But this is failure in itself. The only thing worse than being the fat guy in the pool, is being the fat guy in the pool with a shirt on.

Can't tell you how many times I've been **that** guy. Read this, it might explain your feelings perfectly.
