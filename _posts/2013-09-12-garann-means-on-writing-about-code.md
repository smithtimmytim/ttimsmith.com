---
layout: post
title: Garann Means on Writing about Code
categories: links
custom_type: link
custom_url: http://www.garann.com/dev/2013/how-to-blog-about-code-and-give-zero-fucks/
date: "2013-09-12 12:50:32"
hidden: true
---
>All joking aside, our communities need to hear from people who aren’t the maintainers and conference speakers and web celebrities.[…] The big names create an echo chamber where ideas are safe and popular and failure and being wrong are covered up so no one else can learn from them. We don’t really badly need any more of that crap. We need you.

I just love how Garann writes. I haven't had the chance to talk with her much, but I imagines she writes a lot like she talks, which is awesome. Her and Divya Manian do an [awesome podcast about front-end stuff](http://fripfrap.io/). You should check it out.

Interestingly, this is something I talked about with many people at Blend Conf. Not enough people are writing about their experiences and process. If you finish the article and don't feel the need to write something, we really need to talk.
