---
layout: post
title: An Interview Show Done Right
categories: links
custom_type: link
custom_url: http://www.wnyc.org/shows/heresthething/
date: "2013-08-26 21:26:45"
hidden: true
---
Here's The Thing with Alec Baldwin is my new favorite show. I'm always listening to interview shows in an effort to make my own show better. Baldwin is surprisingly a great host, and pulls back the curtain on some very interesting people.

What's so interesting about these interviews, is the lack of superficial rubbish that usually dominates late-night television, where celebrities answer questions based on what they're supposed to say. It's fascinating to hear the stories of these people who, much like us, are just trying to do the best, and creatively challenging work they can.

My personal favorites are [Kristen Wiig](http://www.wnyc.org/shows/heresthething/2012/apr/09/), [Lena Dunham](http://www.wnyc.org/shows/heresthething/2013/jan/21/), [David Letterman](http://www.wnyc.org/shows/heresthething/2012/jun/18/), and [Lorne Michaels](http://www.wnyc.org/shows/heresthething/2012/jan/30/).
