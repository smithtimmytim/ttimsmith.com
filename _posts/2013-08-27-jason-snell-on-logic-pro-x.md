---
layout: post
title: Jason Snell on Logic Pro X
categories: links
custom_type: link
custom_url: http://www.macworld.com/article/2047106/logic-pro-x-the-right-tool-for-podcasters-.html
date: "2013-08-27 10:45:23"
hidden: true
---
>Would a crazy person like me, who has adopted this music tool for the non-musical business of producing spoken-word audio, benefit from upgrading to Logic Pro X?
>
>After editing several hours of podcasts with Logic Pro X, the answer is a qualified yes. Logic Pro X’s improved interface is easier to use than its predecessor's, and makes it much easier for novice users to discover the program’s many complex features.

Jason Snell's article on Logic Pro X is a pretty good read. He makes a really good point. Many of these audio editing applications are built for musicians, and as podcasters, we find a way for it to work how we need it to. The absence of a good noise reduction plug-in is absolutely ridiculous in a app like this. I know I've needed it on various occasions.[^1]

Either way, I agree that Logic is definitely a step up from Garageband, and if that's what you're currently using, I'd seriously recommend looking into the upgrade. Logic may not be the easiest app to use, but from my experience, no DAW is.


[^1]: I record in a room with horrible acoustics, and lots of echo. Lots of my guests don't record from sound-treated rooms either.
