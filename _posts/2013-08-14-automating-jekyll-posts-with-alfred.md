---
layout: post
title: Automating Jekyll Posts with Alfred
categories: videos
custom_type: video
date: "2013-08-14 15:39:22"
video-embed: <iframe src="http://player.vimeo.com/video/72375755?title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
alias: /videos/automating-jekyll-posts-with-alfred
hidden: true
---
Just a couple days ago, I released some [Jekyll post automaters]({{ site.url}}/links/jekyll-post-automaters/). This screencast teaches you how to use them.

### Video Notes
- [Jekyll Draft Publishing Plugin](http://jeffreysambells.com/2013/02/01/jekyll-draft-publishing-plugin)
- [ttimsmith/Jekyll-Post-Automaters](https://github.com/ttimsmith/Jekyll-Post-Automaters)
