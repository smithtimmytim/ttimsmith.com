---
layout: post
title: Reeder is Back with a New App
categories: links
custom_type: link
custom_url: http://www.theverge.com/2013/9/11/4719890/reeder-2-for-ios-app-leaps-back-into-the-rss-battle
date: "2013-09-12 11:34:39"
hidden: true
---
>Reeder 2 features a new design, support for services like Fever's "Hot List", and a convenient pull-out tab for sharing reminiscent of Windows 8's "Charms" menu, but the app's most important new feature is that it's "universal" — it works on both iPhone and iPad.

Reeder is back! So happy to see Reeder come out with a new app, that works with new services.[^1] Shawn Blanc [says the new app is great](http://shawnblanc.net/2013/09/the-new-reeder-for-ios/), and I trust him. I'll most likely buy and give it a try in the next couple of days.

It's truly amazing to see how the RSS landscape has changed in the past few months. The whole Google Reader shutdown gave everyone a kick in the pants to do a better job with their app, and gave others the opportunity to create new businesses.

[^1]: From what I can see, it works with Feed Wrangler, Feedbin, Feedly, Fever, Readability, and allows you to bypass syncing and just do local RSS subscriptions.
