---
layout: post
title: Netflix Expands Original Content with Stand-up Comedy
categories: links
custom_type: link
custom_url: http://www.theverge.com/2013/8/29/4671028/aziz-ansari-inks-exclusive-deal-with-netflix-for-next-stand-up-special
date: "2013-08-29 12:59:37"
hidden: true
---
>Netflix is opening up another front in its ongoing battle with HBO: comedy. The New York Times reports that Parks and Recreation star Aziz Ansari’s upcoming special Buried Alive will debut on Netflix on November 1st, making it the highest profile stand-up special on the service to date.

Netflix get's it. If I had the money, I'd buy Netflix stock, because they've done nothing but make great decisions in the past year. To think, there was a time when Netflix looked like it would go down in flames, and now is a producer of quality content at an **affordable** price. That's a difficult thing to do.

I can't wait to see what's in store for the company in the next year. I see very good things.
