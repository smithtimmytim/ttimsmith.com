---
layout: page
title: Membership
permalink: /membership/
---
Thank you so much for being interested in supporting me! This page will explain the different levels of support, and why I'm asking for it. I've tried to be as clear and transparent as possible, but if you have any questions, please don't hesitate to [email me](mailto:smith@ttimsmith.com?subject=Membership Question).

[Become a member](http://www.patreon.com/ttimsmith){:.btn.secondary.big.centered-btn}

### Membership Levels
Members of all levels will receive weekly updates containing information and audio clips of upcoming shows, and behind-the-scenes videos on setup and how shows are produced, and updates on how your membership money is being used. As time goes on, I will be developing more member-exclusive benefits.

- **One-Time Donation** - *Any amount*   
   If you'd like to make a one-time donation, you can do so! You'll receive the benefits of a member for one whole month, and the member level benefits depending on your donation amount.
   
   [Donate Here](https://plasso.co/s/XqZS2WQU1A){:.btn.centered-btn.plo-button}

- **Americano** - *$5/month*   
   The benefits for all members plus a personal thank you. You've done something very nice for me. I won't forget it.
   
- **Caramel Latte** - *$19/month*      
   The benefits for all members, a personal thank you, a recording for your podcast or voicemail, and a bundle of sound bites of Tim that you can use as ring tones or alert tones.

- **Latte** - *$20/month*   
   *Limited 0 out of 4 left*{:.highlighted}
   
   The benefits for all members, a personal thank you, a recording for your podcast or voicemail, and a mention on The Daily Radio Column at the beginning of the show. Includes a link to something you'd like to plug.

- **Espresso** - *$100/month*   
   The benefits for all members, a personal thank you, a recording for your podcast or voicemail, and two hours a month of my time. We can jump on Skype and talk podcasting, design, front-end development, life; whatever you'd like.

   Now, I know what you're thinking, "Tim, what happened to the coffee metaphors?" Well… I ran out of them, and Executive Producer best explained what this one was. This level is at a high price *on purpose*. The amount is more than a fifty percent discount on sponsoring all the shows I do, yet includes a lot more benefits than just sponsorship.

### What would I be supporting?
The projects I currently have, but want to spend more time are:

- **For the Record**   
   This is a weekly show that I do that tries to educate and inspire about technology, design, life, and a lot more. It's a complex show, that I love doing. [Here’s one my favorite episodes](http://goodstuff.fm/ftr/6).

   Adam Clark writes on iTunes:

   > I know Tim Smith personally, but I didn't give it 5 stars because he's a friend. For the Record really is a fantastic podcast. Unlike any other podcast I listen to. Extremely high production values and very interesting content.

- ***The Bold Report***   
   *The Bold Report* is a blog that I write about tech, Apple, and more. It's my commentary on current events within the tech industry. I'm no good at predicting things, and I'm for sure no [John Gruber](http://daringfireball.net/) or [Stephen Hackett](http://www.512pixels.net/), but I think the strength of the blog is making the connection between advances of technology and what that does to us as people.

- **The East Wing**   
   This show is a favorite of many that follow me. It's a weekly conversation/interview with people in our industry. We talk about who they are, how the got their start, what they've learned, and where they're going.

   Mark Rondina writes on iTunes:

   > The East Wing is one of the best podcasts out there from front-end development and design. Tim's personality and passion for designing and developing for the web come through in his insightful questions and laid-back demeanor. I've had a chance to listen to a lot of podcasts and keep coming back to Tim's for its content. Great Stuff.

- **The Daily Radio Column**   
   The newest of my shows, The Daily Radio Column has already become a favorite for some. The show serves almost as personal diary where I let people into things, that I probably shouldn't be sharing.

   Austin Grigg writes on iTunes:

   > I enjoy listening to Tim explore things he is wrestling with in his own life and how he invites the listeners into his own musings. Episodes are short and thought provoking.

### Why become a member?
If you're here, you know that I make a living by working freelance. I do design and front-end development.[^1] I'm also really interested in [writing](http://theboldreport.net/) and [podcasting](http://goodstuff.fm/people/ttimsmith). However, there is only so much time, and because those two endeavors don't allow me to support myself, I've had to dedicate more and more time to client work.[^2]

**I'd like all of this to change.**

I want to spend more time on my projects, projects that at their full-speed are providers of content that thousands of people enjoy. To make that possible, I need to be able to pay more of my bills from money generated by these projects. This would allow me to allot more time to my projects, and not be as concerned with client work.

I decided on a membership because expenses are usually monthly, so a monthly membership would give me the money right when I need it.

To summarize, consider becoming a member because you want to support me in creating content, that I'm sure you'll love.

### Processing
The processing and payments of memberships are done through [Patreon](http://www.patreon.com/ttimsmith). If you have questions about this, send me an [email](mailto:smith@ttimsmith.com?subject=Membership Question).

[Become a member](http://www.patreon.com/ttimsmith){:.btn.secondary.big.centered-btn}

[^1]: If you're interested in hiring me, you can find more information about that on the [hire page](/hire/).
[^2]: If you freelance, you most likely understand that it's not always the most stable of incomes.

